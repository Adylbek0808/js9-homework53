import React from 'react';

const Task = props => {
    return (
        <div className="task">
            <p>{props.name}</p>
            <button type="button" onClick={props.remove}>Remove</button>
        </div>
    );
};

export default Task;