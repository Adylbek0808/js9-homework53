import React, { useState } from 'react';
import './App.css';
import Task from './Task/Task'

const App = () => {

  const [tasks, setTasks] = useState([
    { name: "Buy milk", id: "some date 1" },
    { name: "Walk the dog", id: "some date 2" },
    { name: "Got to the bank, get money", id: "some date 3" }
  ]);

  let tasksList = null;
  tasksList =
    <>
      {tasks.map((task, index) => {
        return <Task
          key={task.id}
          name={task.name}
          remove={() => { removeTask(task.id) }}
        >
        </Task>
      })}
    </>
    ;

  const addTask = e => {
    e.preventDefault();
    const tasksCopy = [...tasks];
    const newTask = { name: document.getElementById("newTask").value, id: new Date() }
    tasksCopy.push(newTask);
    setTasks(tasksCopy);
  };

  const removeTask = id => {
    const tasksCopy = [...tasks];
    const index = tasks.findIndex(task => task.id === id);
    tasksCopy.splice(index, 1);
    setTasks(tasksCopy);
  };


  return (
    <div className="App container">
      <h1>Task Manager</h1>
      <form onSubmit={addTask}>
        <input type="text" placeholder="Enter new task" id="newTask" />
        <button type="submit" >Add</button>
      </form>
      <div className="tasksList">
        <h3>Current tasks:</h3>
        {tasksList}
      </div>
    </div>
  );
}

export default App;
